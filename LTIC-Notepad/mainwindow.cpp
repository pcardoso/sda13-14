#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionOpen_triggered()
{
    QString fn_temp = QFileDialog::getOpenFileName(this, "Open file...","","*.txt");

    if (fn_temp != ""){ // cancel wasn't hit!
        filename = fn_temp;
        QFile f(filename);
        f.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&f);
        ui->plainTextEdit->setPlainText(in.readAll());
        f.close();
    }
}

void MainWindow::on_actionSave_triggered()
{
    if (filename == ""){
        filename = QFileDialog::getSaveFileName(this, "Save file...","", ".txt");
    }
    if(filename != ""){
        QFile f(filename);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&f);
        out << ui->plainTextEdit->toPlainText();
        f.close();
    }
}

void MainWindow::on_plainTextEdit_textChanged()
{
    ui->statusBar->showMessage(QString("Character count: %1").arg(ui->plainTextEdit->toPlainText().length()));
}
