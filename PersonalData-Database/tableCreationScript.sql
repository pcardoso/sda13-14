
/*
* Documentation: http://www.sqlite.org/index.html 
*/

-- Create the Persons Table
CREATE TABLE Persons (
	id INTEGER PRIMARY KEY, 
	name VARCHAR(100), 
	email VARCHAR(100), 
	birthdate Date
);


-- Insert some data
INSERT INTO Persons VALUES 
  (NULL, "Joao", 	"joao@ltic.pt", 	"2000-11-25"),
  (NULL, "Margarida", 	"margarida@ltic.pt", 	"2004-06-14");
  

-- check insert operations... 
SELECT * FROM Persons;