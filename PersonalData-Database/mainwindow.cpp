#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //connect to the database
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../data.sqlite3");
    db.open();

    // set a "cursor"
    query = QSqlQuery(db);
    query.exec("SELECT id, name, email, birthdate FROM Persons");

    if (query.isActive()){
        ui->statusBar->showMessage("Database connection: ok");
        query.first();
        FillForm();
    }else{
        ui->statusBar->showMessage(QString("Database connection: error / ") + query.lastError().text());
    }

    ui->Save->hide();

    newrecord = false;
}

MainWindow::~MainWindow()
{
    delete ui;
    db.close();
}


void MainWindow::FillForm()
{
    ui->Name->setText(query.value(1).toString());
    ui->Email->setText(query.value(2).toString());
    ui->BirthDate->setDate(query.value(3).toDate());
}

void MainWindow::on_Previous_clicked()
{
    if (!query.previous())
        query.last();
    FillForm();
}

void MainWindow::on_Next_clicked()
{
    if(!query.next())
        query.first();
    FillForm();
}

void MainWindow::on_Save_clicked()
{
    if (! newrecord){
        if (ui->Name->text().trimmed() == ""){
            ui->statusBar->showMessage("Name is empty!");
        }else{

            QSqlQuery updateQuery = QSqlQuery(db);
            updateQuery.prepare("UPDATE persons SET name = :name, email = :email, birthdate = :birthdate WHERE id = :id");
            updateQuery.bindValue(":id", query.value(0));
            updateQuery.bindValue(":name", ui->Name->text());
            updateQuery.bindValue(":email", ui->Email->text());
            updateQuery.bindValue(":birthdate", ui->BirthDate->text());

            if(!updateQuery.exec())
                ui->statusBar->showMessage(QString("Query (update) error: ") + updateQuery.lastError().text());
            else
                ui->statusBar->showMessage("Update: ok");

            int currentPid = query.value(0).toInt();
            query.exec("SELECT id, name, email, birthdate FROM Persons");
            query.first();
            while (query.value(0).toInt() != currentPid) query.next();
            FillForm();
       }
    }else{
        if (ui->Name->text().trimmed() == ""){
            ui->statusBar->showMessage("Name is empty!");
        }else{
            newrecord = false;
            QSqlQuery updateQuery = QSqlQuery(db);
            updateQuery.prepare("INSERT INTO persons (name, email, birthdate) VALUES (:name, :email, :birthdate)");
            updateQuery.bindValue(":name", ui->Name->text());
            updateQuery.bindValue(":email", ui->Email->text());
            updateQuery.bindValue(":birthdate", ui->BirthDate->text());

            if(!updateQuery.exec())
                ui->statusBar->showMessage(QString("Query (insert) error: ") + updateQuery.lastError().text());
            else
                ui->statusBar->showMessage("Insert: ok");


            query.exec("SELECT id, name, email, birthdate FROM Persons");
            query.last();
            FillForm();
        }
    }
}



void MainWindow::on_New_clicked()
{
    newrecord = true;
}

void MainWindow::on_Cancel_clicked()
{
    FillForm();
    newrecord = false;
}
