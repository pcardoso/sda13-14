#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../data.sqlite3");
    db.open();

    QSqlQuery q(db);
    q.exec("SELECT COUNT(id) FROM Persons");
    q.first();
    ui->tableWidget->setRowCount(q.value(0).toInt());

    q.exec("SELECT id, name, email FROM Persons");
    int r = 0;

    for (q.first(); q.isValid(); q.next(), ++r){
        for (int c = 0; c < 3; c++)
            ui->tableWidget->setItem(
                        r, c,
                        new QTableWidgetItem(q.value(c).toString())
                    );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    db.close();
}

void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{
    dialog = new form(this);
    dialog->setModal(true);
    dialog->show();

    connect(this, SIGNAL(sendData(QString, QString)),
            dialog, SLOT(receiveData(QString, QString))
            );
    connect(dialog, SIGNAL(sendFormData(QString, QString)),
            this, SLOT(receiveFormData(QString, QString))
            );

    emit sendData(ui->tableWidget->item(row, 1)->text(),
                  ui->tableWidget->item(row, 2)->text()
                 );
<<<<<<< HEAD

    newrow = false;
=======
>>>>>>> df49105e1eeb5e09aea7098a10ca34ae4b65abcb
}


void MainWindow::receiveFormData(QString name, QString email){
<<<<<<< HEAD
    if(!newrow){
        int r = ui->tableWidget->currentRow();

        QTableWidgetItem *iname = ui->tableWidget->item(r, 1);
        QTableWidgetItem *iemail = ui->tableWidget->item(r, 2);

        iname->setText(name);
        iemail->setText(email);
    }else{
        int r = ui->tableWidget->rowCount();
        ui->tableWidget->setRowCount( ++r );
        ui->tableWidget->setItem(
                    r-1, 1, new QTableWidgetItem( name )
                    );
        ui->tableWidget->setItem(
                    r-1, 2, new QTableWidgetItem( email )
                    );
    }
}

void MainWindow::on_newbt_clicked()
{
    dialog = new form(this);
    dialog->setModal(true);
    dialog->show();

    connect(this, SIGNAL(sendData(QString,QString)),
            dialog, SLOT(receiveData(QString,QString)));
    connect(dialog, SIGNAL(sendFormData(QString,QString)),
            this, SLOT(receiveFormData(QString,QString)));

    emit sendData("Preencha o nome", "Preencha o email");

    newrow = true;

=======
    int r = ui->tableWidget->currentRow();

    QTableWidgetItem *iname = ui->tableWidget->item(r, 1);
    QTableWidgetItem *iemail = ui->tableWidget->item(r, 2);

    iname->setText(name);
    iemail->setText(email);
>>>>>>> df49105e1eeb5e09aea7098a10ca34ae4b65abcb
}
