#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_swap_clicked()
{
    QString temp = ui->T_A->text();
    ui->T_A->setText(ui->T_B->text());
    ui->T_B->setText(temp);
}

