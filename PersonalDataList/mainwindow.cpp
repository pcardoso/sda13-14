#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    data.append(PersonalData(QString("João"), QString("joao@ltic.pt"), QString("João is ...")));
    data.append(PersonalData(QString("Margarida"), QString("margarida@ltic.pt"), QString("Margarida is ...")));

    registry_number = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_LoginBt_clicked()
{
    if (ui->Login->text() == "LTIC" && ui->Password->text() == "123"){
        ui->LogMessage->setText("OK!");
        ui->LoginPage->setVisible(false);
        ui->DataPage->setVisible(true);
        FillForm();
    }else{
        ui->LogMessage->setText("Login error!");
    }

}

void MainWindow::on_LogoutBt_clicked()
{
    ui->Password->setText("");
    ui->LoginPage->setVisible(true);
    ui->DataPage->setVisible(false);
}

void MainWindow::on_Previous_clicked()
{
    registry_number--;
    if(registry_number < 0)
        registry_number = data.length() - 1;
    FillForm();

}

void MainWindow::FillForm(){
    ui->Name->setText(data[registry_number].getName());
    ui->Email->setText(data[registry_number].getEmail());
    ui->Observations->setPlainText(data[registry_number].getObservation());
}

void MainWindow::on_Next_clicked()
{
    registry_number++;
    if (registry_number == data.length())
        registry_number = 0;
    FillForm();
}
