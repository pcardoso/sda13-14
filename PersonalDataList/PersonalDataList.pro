#-------------------------------------------------
#
# Project created by QtCreator 2014-04-28T20:13:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PersonalDataList
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    personaldata.cpp

HEADERS  += mainwindow.h \
    personaldata.h

FORMS    += mainwindow.ui
