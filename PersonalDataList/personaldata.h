#ifndef PERSONALDATA_H
#define PERSONALDATA_H

#include <QString>

class PersonalData
{
private:
    QString Name;
    QString Email;
    QString Observation;
public:
    PersonalData();
    PersonalData(QString Name, QString Email, QString Observation);
    QString getName(){return Name;}
    QString getEmail(){return Email;}
    QString getObservation(){return Observation;}

};

#endif // PERSONALDATA_H
