#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "personaldata.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /// action if button LoginBt is clicked
    /// checks if 'login' and 'password' are correct (to-do: connecto to database)
    void on_LoginBt_clicked();
    /// action if button LogoutBt is clicked
    /// do the logout returning to the 'login window'
    void on_LogoutBt_clicked();
    /// action for button 'previous' clicked
    void on_Previous_clicked();
    /// action for button 'next' clicked
    void on_Next_clicked();
private:
    Ui::MainWindow *ui;

    ///personal data vector
    QVector<PersonalData> data;
    /// current register index
    int registry_number;
    /// private function to fill the form with person's data
    void FillForm();
};
#endif // MAINWINDOW_H
