#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_tableWidget_cellChanged(int row, int column);

    void on_new_2_clicked();

    void on_delete_2_clicked();

    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;
    QSqlDatabase db;

    bool loading;

    void FillTable();
};

#endif // MAINWINDOW_H
