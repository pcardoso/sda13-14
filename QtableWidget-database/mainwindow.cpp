#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../data.sqlite3");

    if(!db.open()){
        qDebug() << db.lastError().text();
    }

    ui->tableWidget->hideColumn(0);
    FillTable();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::FillTable(){

    loading = true;
    int num_rows, r, c;
    QSqlQuery q(db);

    //get the number of rows
    if(!q.exec("SELECT count(id) as num_rows FROM Persons")) qDebug() << q.lastError().text();
    q.first();
    num_rows = q.value(0).toInt();

    ui->tableWidget->setRowCount(num_rows);

    if(!q.exec("SELECT id, name, email, birthdate FROM Persons ORDER BY id")) qDebug() << q.lastError().text();
    for(r = 0, q.first(); q.isValid(); q.next(), ++r){
        for(c = 0; c < 4; ++c){
            ui->tableWidget->setItem(r,c, new QTableWidgetItem(q.value(c).toString()));
        }
    }
    loading = false;
}

void MainWindow::on_tableWidget_cellChanged(int row, int column)
{
    if (loading) return ;

    QSqlQuery q(db);

    q.prepare("UPDATE Persons SET name = :n, email = :e, birthdate = :bd WHERE id = :id");
    q.bindValue(":n", ui->tableWidget->item(row,1)->text());
    q.bindValue(":e", ui->tableWidget->item(row,2)->text());
    q.bindValue(":bd", ui->tableWidget->item(row,3)->text());
    q.bindValue(":id", ui->tableWidget->item(row, 0)->text().toInt());

    if(!q.exec())  qDebug() << q.lastError().text();
}

void MainWindow::on_new_2_clicked()
{
    QSqlQuery q(db);
    if(!q.exec("INSERT INTO Persons (name, email, birthdate) VALUES ('', '', '')")) qDebug() << q.lastError().text();
    FillTable();
}

void MainWindow::on_delete_2_clicked()
{
    int del_id = ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->text().toInt();

    QSqlQuery q(db);
    q.prepare("DELETE FROM Persons WHERE id = :id");
    q.bindValue(":id", del_id);
    if(!q.exec())  qDebug() << q.lastError().text();

    FillTable();
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}
